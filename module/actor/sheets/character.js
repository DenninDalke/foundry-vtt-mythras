import ActorSheet5e from "./base.js";

/**
 * An Actor sheet for player character type actors in the Mythras system.
 * Extends the base ActorSheet5e class.
 * @type {ActorSheet5e}
 */
export default class ActorSheet5eCharacter extends ActorSheet5e {

  /**
   * Define default rendering options for the NPC sheet
   * @return {Object}
   */
	static get defaultOptions() {
	  return mergeObject(super.defaultOptions, {
      classes: ["mythras", "sheet", "actor", "character"],
      width: 720,
      height: 680
    });
  }

  /* -------------------------------------------- */
  /*  Rendering                                   */
  /* -------------------------------------------- */

  /**
   * Get the correct HTML template path to use for rendering this particular sheet
   * @type {String}
   */
  get template() {
    if ( !game.user.isGM && this.actor.limited ) return "systems/mythras/templates/actors/limited-sheet.html";
    return "systems/mythras/templates/actors/character-sheet.html";
  }

  /* -------------------------------------------- */

  /**
   * Add some extra data when rendering the sheet to reduce the amount of logic required within the template.
   */
  getData() {
    const sheetData = super.getData();

    // Temporary HP
    let hp = sheetData.data.attributes.hp;
    if (hp.temp === 0) delete hp.temp;
    if (hp.tempmax === 0) delete hp.tempmax;

    // Resources
    sheetData["resources"] = ["primary", "secondary", "tertiary"].reduce((arr, r) => {
      const res = sheetData.data.resources[r] || {};
      res.name = r;
      res.placeholder = game.i18n.localize("MYTHRAS.Resource"+r.titleCase());
      if (res && res.value === 0) delete res.value;
      if (res && res.max === 0) delete res.max;
      return arr.concat([res]);
    }, []);

    // Experience Tracking
    sheetData["classLabels"] = this.actor.itemTypes.class.map(c => c.name).join(", ");

    // Return data for rendering
    return sheetData;
  }

  /* -------------------------------------------- */

  /**
   * Organize and classify Owned Items for Character sheets
   * @private
   */
  _prepareItems(data) {

    // Categorize items as inventory, spellbook, features, and classes
    const inventory = {
      weapon: { label: "MYTHRAS.ItemTypeWeaponPl", items: [], dataset: {type: "weapon"} },
      equipment: { label: "MYTHRAS.ItemTypeEquipmentPl", items: [], dataset: {type: "equipment"} },
      consumable: { label: "MYTHRAS.ItemTypeConsumablePl", items: [], dataset: {type: "consumable"} },
      tool: { label: "MYTHRAS.ItemTypeToolPl", items: [], dataset: {type: "tool"} },
      backpack: { label: "MYTHRAS.ItemTypeContainerPl", items: [], dataset: {type: "backpack"} },
      loot: { label: "MYTHRAS.ItemTypeLootPl", items: [], dataset: {type: "loot"} },
      skills: { label: "MYTHRAS.ItemTypeSkillPl", items: [], dataset: {type: "skills"} }
    };

    // Partition items by category
    let [items, spells, feats, classes, skills] = data.items.reduce((arr, item) => {

      // Item details
      if (item.type === "skill") {
        item.img = item.img || "icons/svg/d20-grey.svg";
      }
      else {
        item.img = item.img || DEFAULT_TOKEN;
      }
      item.isStack = item.data.quantity ? item.data.quantity > 1 : false;

      // Item usage
      item.hasUses = item.data.uses && (item.data.uses.max > 0);
      item.isOnCooldown = item.data.recharge && !!item.data.recharge.value && (item.data.recharge.charged === false);
      item.isDepleted = item.isOnCooldown && (item.data.uses.per && (item.data.uses.value > 0));
      item.hasTarget = !!item.data.target && !(["none",""].includes(item.data.target.type));

      // Item toggle state
      this._prepareItemToggleState(item);

      // Classify items into types
      if ( item.type === "spell" ) arr[1].push(item);
      else if ( item.type === "feat" ) arr[2].push(item);
      else if ( item.type === "class" ) arr[3].push(item);
      else if ( item.type === "skill" ) arr[4].push(item);
      else if ( Object.keys(inventory).includes(item.type ) ) arr[0].push(item);
      return arr;
    }, [[], [], [], [], []]);

    // Apply active item filters
    items = this._filterItems(items, this._filters.inventory);
    spells = this._filterItems(spells, this._filters.spellbook);
    feats = this._filterItems(feats, this._filters.features);
    skills = this._filterItems(skills, this._filters.skills);

    // Organize Spellbook and count the number of prepared spells (excluding always, at will, etc...)
    const spellbook = this._prepareSpellbook(data, spells);
    const nPrepared = spells.filter(s => {
      return (s.data.level > 0) && (s.data.preparation.mode === "prepared") && s.data.preparation.prepared;
    }).length;

    // Organize Inventory
    let totalWeight = 0;
    for ( let i of items ) {
      i.data.quantity = i.data.quantity || 0;
      i.data.weight = i.data.weight || 0;
      i.totalWeight = Math.round(i.data.quantity * i.data.weight * 10) / 10;
      inventory[i.type].items.push(i);
      totalWeight += i.totalWeight;
    }
    data.data.attributes.encumbrance = this._computeEncumbrance(totalWeight, data);

    // Organize Features
    // const features = {
    //   classes: { label: "MYTHRAS.ItemTypeClassPl", items: [], hasActions: false, dataset: {type: "class"}, isClass: true },
    //   active: { label: "MYTHRAS.FeatureActive", items: [], hasActions: true, dataset: {type: "feat", "activation.type": "action"} },
    //   passive: { label: "MYTHRAS.FeaturePassive", items: [], hasActions: false, dataset: {type: "feat"} },
    // };
    // for ( let f of feats ) {
    //   if ( f.data.activation.type ) features.active.items.push(f);
    //   else features.passive.items.push(f);
    // }
    // classes.sort((a, b) => b.levels - a.levels);
    // features.classes.items = classes;

    // Prepare Skills
    const skillCategories = this._prepareSkills(data, skills);

    // Assign and return
    data.inventory = Object.values(inventory);
    data.spellbook = spellbook;
    data.preparedSpells = nPrepared;
    //data.features = Object.values(features);
    data.skills = Object.values(skillCategories);
  }

  /* -------------------------------------------- */

  /**
   * Organize and classify Owned Items for Character sheets
   * @private
   */
  _prepareSkills(data, skills) {
    const skillCategories = {
      standardSkills: { label: "MYTHRAS.Skills.StandardSkills.Title", items: [], dataset: {type: "standardSkill"}, isExpanded: this.actor.data.data.uiStates.standardSkillIsExpanded },
      professionalSkills: { label: "MYTHRAS.Skills.ProfessionalSkills.Title", items: [], dataset: {type: "professionalSkill"}, isExpanded: this.actor.data.data.uiStates.professionalSkillIsExpanded }
    }

    var skillsToCreate = Object.keys(CONFIG.MYTHRAS.standardSkills);
    const characteristics = data.actor.data.characteristics;

    for ( let skill of skills ) {
      const skillData = skill.data;
      const characteristic1 = parseInt(characteristics[skillData.characteristic1].total);
      const characteristic2 = parseInt(characteristics[skillData.characteristic2].total);
      const training = parseInt(skillData.training);
      skill.total = characteristic1 + characteristic2 + training;

      if (skillData.localizedNameValue === undefined) {
        skillData.localizedNameValue = game.i18n.localize(skillData.localizedName);
      }

      let indexOf = skillsToCreate.indexOf(skill.name);
      if (indexOf != -1) {
         skillsToCreate.splice(indexOf, 1);
         skill.isDefault = true;
      }

      skill.editable = this.actor.owner && !skill.isDefault;

      if ( skill.data.isStandard ) skillCategories.standardSkills.items.push(skill);
      else skillCategories.professionalSkills.items.push(skill);
    }

    // Sort skills alphabetically:
    skillCategories.standardSkills.items.sort(function (a, b) {
      const aName = a.data.localizedNameValue ? a.data.localizedNameValue : a.name;
      const bName = b.data.localizedNameValue ? b.data.localizedNameValue : b.name;

      if (aName > bName) {
        return 1;
      }
      if (aName < bName) {
        return -1;
      }
      return 0;
    });

    let ownerActor =  game.actors.get(this.actor._id);
    let skillItems = [];

    for (let key of skillsToCreate) {
      let skill = CONFIG.MYTHRAS.standardSkills[key];
      skillItems.push({
        "name": key,
        "img": "icons/svg/d20-grey.svg",
        "data.localizedName": skill.localizationTitleKey,
        "data.localizedDescription": skill.localizationDescriptionKey,
        "data.characteristic1": skill.characteristic1,
        "data.characteristic2": skill.characteristic2,
        "type": "skill"
      });
    }

    if (skillItems.length > 0) {
      ownerActor.createEmbeddedEntity("OwnedItem", skillItems);
    }

    return skillCategories;
  }

  /* -------------------------------------------- */

  /**
   * A helper method to establish the displayed preparation state for an item
   * @param {Item} item
   * @private
   */
  _prepareItemToggleState(item) {
    if (item.type === "spell") {
      const isAlways = getProperty(item.data, "preparation.mode") === "always";
      const isPrepared =  getProperty(item.data, "preparation.prepared");
      item.toggleClass = isPrepared ? "active" : "";
      if ( isAlways ) item.toggleClass = "fixed";
      if ( isAlways ) item.toggleTitle = CONFIG.MYTHRAS.spellPreparationModes.always;
      else if ( isPrepared ) item.toggleTitle = CONFIG.MYTHRAS.spellPreparationModes.prepared;
      else item.toggleTitle = game.i18n.localize("MYTHRAS.SpellUnprepared");
    }
    else {
      const isActive = getProperty(item.data, "equipped");
      item.toggleClass = isActive ? "active" : "";
      item.toggleTitle = game.i18n.localize(isActive ? "MYTHRAS.Equipped" : "MYTHRAS.Unequipped");
    }
  }

  /* -------------------------------------------- */

  /**
   * Compute the level and percentage of encumbrance for an Actor.
   *
   * Optionally include the weight of carried currency across all denominations by applying the standard rule
   * from the PHB pg. 143
   *
   * @param {Number} totalWeight    The cumulative item weight from inventory items
   * @param {Object} actorData      The data object for the Actor being rendered
   * @return {Object}               An object describing the character's encumbrance level
   * @private
   */
  _computeEncumbrance(totalWeight, actorData) {

    // Encumbrance classes
    let mod = {
      tiny: 0.5,
      sm: 1,
      med: 1,
      lg: 2,
      huge: 4,
      grg: 8
    }[actorData.data.traits.size] || 1;

    // Apply Powerful Build feat
    if ( this.actor.getFlag("mythras", "powerfulBuild") ) mod = Math.min(mod * 2, 8);

    // Add Currency Weight
    if ( game.settings.get("mythras", "currencyWeight") ) {
      const currency = actorData.data.currency;
      const numCoins = Object.values(currency).reduce((val, denom) => val += denom, 0);
      totalWeight += numCoins / CONFIG.MYTHRAS.encumbrance.currencyPerWeight;
    }

    // Compute Encumbrance percentage
    const enc = {
      max: actorData.data.abilities.str.value * CONFIG.MYTHRAS.encumbrance.strMultiplier * mod,
      value: Math.round(totalWeight * 10) / 10,
    };
    enc.pct = Math.min(enc.value * 100 / enc.max, 99);
    enc.encumbered = enc.pct > (2/3);
    return enc;
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers
  /* -------------------------------------------- */

  /**
   * Activate event listeners using the prepared sheet HTML
   * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
   */
	activateListeners(html) {
    super.activateListeners(html);
    if ( !this.options.editable ) return;

    // Inventory Functions
    html.find(".currency-convert").click(this._onConvertCurrency.bind(this));

    // Item State Toggling
    html.find('.item-toggle').click(this._onToggleItem.bind(this));

    // Short and Long Rest
    html.find('.short-rest').click(this._onShortRest.bind(this));
    html.find('.long-rest').click(this._onLongRest.bind(this));

    // Death saving throws
    html.find('.death-save').click(this._onDeathSave.bind(this));
  }

  /* -------------------------------------------- */

  /**
   * Handle rolling a death saving throw for the Character
   * @param {MouseEvent} event    The originating click event
   * @private
   */
  _onDeathSave(event) {
    event.preventDefault();
    return this.actor.rollDeathSave({event: event});
  }

  /* -------------------------------------------- */


  /**
   * Handle toggling the state of an Owned Item within the Actor
   * @param {Event} event   The triggering click event
   * @private
   */
  _onToggleItem(event) {
    event.preventDefault();
    const itemId = event.currentTarget.closest(".item").dataset.itemId;
    const item = this.actor.getOwnedItem(itemId);
    const attr = item.data.type === "spell" ? "data.preparation.prepared" : "data.equipped";
    return item.update({[attr]: !getProperty(item.data, attr)});
  }

  /* -------------------------------------------- */

  /**
   * Take a short rest, calling the relevant function on the Actor instance
   * @param {Event} event   The triggering click event
   * @private
   */
  async _onShortRest(event) {
    event.preventDefault();
    await this._onSubmit(event);
    return this.actor.shortRest();
  }

  /* -------------------------------------------- */

  /**
   * Take a long rest, calling the relevant function on the Actor instance
   * @param {Event} event   The triggering click event
   * @private
   */
  async _onLongRest(event) {
    event.preventDefault();
    await this._onSubmit(event);
    return this.actor.longRest();
  }

  /* -------------------------------------------- */

  /**
   * Handle mouse click events to convert currency to the highest possible denomination
   * @param {MouseEvent} event    The originating click event
   * @private
   */
  async _onConvertCurrency(event) {
    event.preventDefault();
    return Dialog.confirm({
      title: `${game.i18n.localize("MYTHRAS.CurrencyConvert")}`,
      content: `<p>${game.i18n.localize("MYTHRAS.CurrencyConvertHint")}</p>`,
      yes: () => this.actor.convertCurrency()
    });
  }
}
