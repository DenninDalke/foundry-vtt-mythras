// Namespace Mythras Configuration Values
export const MYTHRAS = {};

// ASCII Artwork
MYTHRAS.ASCII = `_______________________________
______      ______ _____ _____ 
|  _  \\___  |  _  \\  ___|  ___|
| | | ( _ ) | | | |___ \\| |__  
| | | / _ \\/\\ | | |   \\ \\  __| 
| |/ / (_>  < |/ //\\__/ / |___ 
|___/ \\___/\\/___/ \\____/\\____/
_______________________________`;


/**
 * The set of Ability Scores used within the system
 * @type {Object}
 */
MYTHRAS.abilities = {
  "str": "MYTHRAS.AbilityStr",
  "dex": "MYTHRAS.AbilityDex",
  "con": "MYTHRAS.AbilityCon",
  "int": "MYTHRAS.AbilityInt",
  "wis": "MYTHRAS.AbilityWis",
  "cha": "MYTHRAS.AbilityCha"
};

/* -------------------------------------------- */

/**
 * Character alignment options
 * @type {Object}
 */
MYTHRAS.alignments = {
  'lg': "MYTHRAS.AlignmentLG",
  'ng': "MYTHRAS.AlignmentNG",
  'cg': "MYTHRAS.AlignmentCG",
  'ln': "MYTHRAS.AlignmentLN",
  'tn': "MYTHRAS.AlignmentTN",
  'cn': "MYTHRAS.AlignmentCN",
  'le': "MYTHRAS.AlignmentLE",
  'ne': "MYTHRAS.AlignmentNE",
  'ce': "MYTHRAS.AlignmentCE"
};


MYTHRAS.weaponProficiencies = {
  "sim": "MYTHRAS.WeaponSimpleProficiency",
  "mar": "MYTHRAS.WeaponMartialProficiency"
};

MYTHRAS.toolProficiencies = {
  "art": "MYTHRAS.ToolArtisans",
  "disg": "MYTHRAS.ToolDisguiseKit",
  "forg": "MYTHRAS.ToolForgeryKit",
  "game": "MYTHRAS.ToolGamingSet",
  "herb": "MYTHRAS.ToolHerbalismKit",
  "music": "MYTHRAS.ToolMusicalInstrument",
  "navg": "MYTHRAS.ToolNavigators",
  "pois": "MYTHRAS.ToolPoisonersKit",
  "thief": "MYTHRAS.ToolThieves",
  "vehicle": "MYTHRAS.ToolVehicle"
};


/* -------------------------------------------- */

/**
 * This Object defines the various lengths of time which can occur in Mythras
 * @type {Object}
 */
MYTHRAS.timePeriods = {
  "inst": "MYTHRAS.TimeInst",
  "turn": "MYTHRAS.TimeTurn",
  "round": "MYTHRAS.TimeRound",
  "minute": "MYTHRAS.TimeMinute",
  "hour": "MYTHRAS.TimeHour",
  "day": "MYTHRAS.TimeDay",
  "month": "MYTHRAS.TimeMonth",
  "year": "MYTHRAS.TimeYear",
  "perm": "MYTHRAS.TimePerm",
  "spec": "MYTHRAS.Special"
};


/* -------------------------------------------- */

/**
 * This describes the ways that an ability can be activated
 * @type {Object}
 */
MYTHRAS.abilityActivationTypes = {
  "none": "MYTHRAS.None",
  "action": "MYTHRAS.Action",
  "bonus": "MYTHRAS.BonusAction",
  "reaction": "MYTHRAS.Reaction",
  "minute": MYTHRAS.timePeriods.minute,
  "hour": MYTHRAS.timePeriods.hour,
  "day": MYTHRAS.timePeriods.day,
  "special": MYTHRAS.timePeriods.spec,
  "legendary": "MYTHRAS.LegAct",
  "lair": "MYTHRAS.LairAct"
};

/* -------------------------------------------- */


MYTHRAS.abilityConsumptionTypes = {
  "ammo": "MYTHRAS.ConsumeAmmunition",
  "attribute": "MYTHRAS.ConsumeAttribute",
  "material": "MYTHRAS.ConsumeMaterial",
  "charges": "MYTHRAS.ConsumeCharges"
};


/* -------------------------------------------- */

// Creature Sizes
MYTHRAS.actorSizes = {
  "tiny": "MYTHRAS.SizeTiny",
  "sm": "MYTHRAS.SizeSmall",
  "med": "MYTHRAS.SizeMedium",
  "lg": "MYTHRAS.SizeLarge",
  "huge": "MYTHRAS.SizeHuge",
  "grg": "MYTHRAS.SizeGargantuan"
};

MYTHRAS.tokenSizes = {
  "tiny": 1,
  "sm": 1,
  "med": 1,
  "lg": 2,
  "huge": 3,
  "grg": 4
};

/* -------------------------------------------- */

/**
 * Classification types for item action types
 * @type {Object}
 */
MYTHRAS.itemActionTypes = {
  "mwak": "MYTHRAS.ActionMWAK",
  "rwak": "MYTHRAS.ActionRWAK",
  "msak": "MYTHRAS.ActionMSAK",
  "rsak": "MYTHRAS.ActionRSAK",
  "save": "MYTHRAS.ActionSave",
  "heal": "MYTHRAS.ActionHeal",
  "abil": "MYTHRAS.ActionAbil",
  "util": "MYTHRAS.ActionUtil",
  "other": "MYTHRAS.ActionOther"
};

/* -------------------------------------------- */

MYTHRAS.itemCapacityTypes = {
  "items": "MYTHRAS.ItemContainerCapacityItems",
  "weight": "MYTHRAS.ItemContainerCapacityWeight"
};

/* -------------------------------------------- */

/**
 * Enumerate the lengths of time over which an item can have limited use ability
 * @type {Object}
 */
MYTHRAS.limitedUsePeriods = {
  "sr": "MYTHRAS.ShortRest",
  "lr": "MYTHRAS.LongRest",
  "day": "MYTHRAS.Day",
  "charges": "MYTHRAS.Charges"
};


/* -------------------------------------------- */

/**
 * The set of equipment types for armor, clothing, and other objects which can ber worn by the character
 * @type {Object}
 */
MYTHRAS.equipmentTypes = {
  "light": "MYTHRAS.EquipmentLight",
  "medium": "MYTHRAS.EquipmentMedium",
  "heavy": "MYTHRAS.EquipmentHeavy",
  "bonus": "MYTHRAS.EquipmentBonus",
  "natural": "MYTHRAS.EquipmentNatural",
  "shield": "MYTHRAS.EquipmentShield",
  "clothing": "MYTHRAS.EquipmentClothing",
  "trinket": "MYTHRAS.EquipmentTrinket"
};


/* -------------------------------------------- */

/**
 * The set of Armor Proficiencies which a character may have
 * @type {Object}
 */
MYTHRAS.armorProficiencies = {
  "lgt": MYTHRAS.equipmentTypes.light,
  "med": MYTHRAS.equipmentTypes.medium,
  "hvy": MYTHRAS.equipmentTypes.heavy,
  "shl": "MYTHRAS.EquipmentShieldProficiency"
};


/* -------------------------------------------- */

/**
 * Enumerate the valid consumable types which are recognized by the system
 * @type {Object}
 */
MYTHRAS.consumableTypes = {
  "ammo": "MYTHRAS.ConsumableAmmunition",
  "potion": "MYTHRAS.ConsumablePotion",
  "poison": "MYTHRAS.ConsumablePoison",
  "food": "MYTHRAS.ConsumableFood",
  "scroll": "MYTHRAS.ConsumableScroll",
  "wand": "MYTHRAS.ConsumableWand",
  "rod": "MYTHRAS.ConsumableRod",
  "trinket": "MYTHRAS.ConsumableTrinket"
};

/* -------------------------------------------- */

/**
 * The valid currency denominations supported by the 5e system
 * @type {Object}
 */
MYTHRAS.currencies = {
  "pp": "MYTHRAS.CurrencyPP",
  "gp": "MYTHRAS.CurrencyGP",
  "ep": "MYTHRAS.CurrencyEP",
  "sp": "MYTHRAS.CurrencySP",
  "cp": "MYTHRAS.CurrencyCP",
};

/* -------------------------------------------- */


// Damage Types
MYTHRAS.damageTypes = {
  "acid": "MYTHRAS.DamageAcid",
  "bludgeoning": "MYTHRAS.DamageBludgeoning",
  "cold": "MYTHRAS.DamageCold",
  "fire": "MYTHRAS.DamageFire",
  "force": "MYTHRAS.DamageForce",
  "lightning": "MYTHRAS.DamageLightning",
  "necrotic": "MYTHRAS.DamageNecrotic",
  "piercing": "MYTHRAS.DamagePiercing",
  "poison": "MYTHRAS.DamagePoison",
  "psychic": "MYTHRAS.DamagePsychic",
  "radiant": "MYTHRAS.DamageRadiant",
  "slashing": "MYTHRAS.DamageSlashing",
  "thunder": "MYTHRAS.DamageThunder"
};

/* -------------------------------------------- */

MYTHRAS.distanceUnits = {
  "none": "MYTHRAS.None",
  "self": "MYTHRAS.DistSelf",
  "touch": "MYTHRAS.DistTouch",
  "ft": "MYTHRAS.DistFt",
  "mi": "MYTHRAS.DistMi",
  "spec": "MYTHRAS.Special",
  "any": "MYTHRAS.DistAny"
};

/* -------------------------------------------- */


/**
 * Configure aspects of encumbrance calculation so that it could be configured by modules
 * @type {Object}
 */
MYTHRAS.encumbrance = {
  currencyPerWeight: 50,
  strMultiplier: 15
};

/* -------------------------------------------- */

/**
 * This Object defines the types of single or area targets which can be applied in Mythras
 * @type {Object}
 */
MYTHRAS.targetTypes = {
  "none": "MYTHRAS.None",
  "self": "MYTHRAS.TargetSelf",
  "creature": "MYTHRAS.TargetCreature",
  "ally": "MYTHRAS.TargetAlly",
  "enemy": "MYTHRAS.TargetEnemy",
  "object": "MYTHRAS.TargetObject",
  "space": "MYTHRAS.TargetSpace",
  "radius": "MYTHRAS.TargetRadius",
  "sphere": "MYTHRAS.TargetSphere",
  "cylinder": "MYTHRAS.TargetCylinder",
  "cone": "MYTHRAS.TargetCone",
  "square": "MYTHRAS.TargetSquare",
  "cube": "MYTHRAS.TargetCube",
  "line": "MYTHRAS.TargetLine",
  "wall": "MYTHRAS.TargetWall"
};


/* -------------------------------------------- */


/**
 * Map the subset of target types which produce a template area of effect
 * The keys are MYTHRAS target types and the values are MeasuredTemplate shape types
 * @type {Object}
 */
MYTHRAS.areaTargetTypes = {
  cone: "cone",
  cube: "rect",
  cylinder: "circle",
  line: "ray",
  radius: "circle",
  sphere: "circle",
  square: "rect",
  wall: "ray"
};


/* -------------------------------------------- */

// Healing Types
MYTHRAS.healingTypes = {
  "healing": "MYTHRAS.Healing",
  "temphp": "MYTHRAS.HealingTemp"
};


/* -------------------------------------------- */


/**
 * Enumerate the denominations of hit dice which can apply to classes in the Mythras system
 * @type {Array.<string>}
 */
MYTHRAS.hitDieTypes = ["d6", "d8", "d10", "d12"];


/* -------------------------------------------- */

/**
 * Character senses options
 * @type {Object}
 */
MYTHRAS.senses = {
  "bs": "MYTHRAS.SenseBS",
  "dv": "MYTHRAS.SenseDV",
  "ts": "MYTHRAS.SenseTS",
  "tr": "MYTHRAS.SenseTR"
};


/* -------------------------------------------- */

/**
 * The set of skill which can be trained in Mythras
 * @type {Object}
 */
MYTHRAS.standardSkills = {
  "athletics": {
    "localizationTitleKey": "MYTHRAS.Skills.Athletics.Title",
    "localizationDescriptionKey": "MYTHRAS.Skills.Athletics.Description",
    "characteristic1": "strength",
    "characteristic2": "dexterity"
  },
  "boating": {
    "localizationTitleKey": "MYTHRAS.Skills.Boating.Title",
    "localizationDescriptionKey": "MYTHRAS.Skills.Boating.Description",
    "characteristic1": "strength",
    "characteristic2": "constitution"
  },
  "brawn": {
    "localizationTitleKey": "MYTHRAS.Skills.Brawn.Title",
    "localizationDescriptionKey": "MYTHRAS.Skills.Brawn.Description",
    "characteristic1": "strength",
    "characteristic2": "size"
  },
  "conceal": {
    "localizationTitleKey": "MYTHRAS.Skills.Conceal.Title",
    "localizationDescriptionKey": "MYTHRAS.Skills.Conceal.Description",
    "characteristic1": "dexterity",
    "characteristic2": "power"
  },
  "customs": {
      "localizationTitleKey": "MYTHRAS.Skills.Customs.Title",
      "localizationDescriptionKey": "MYTHRAS.Skills.Customs.Description",
      "characteristic1": "intelligence",
    "characteristic2": "intelligence"
  },
  "dance": {
    "localizationTitleKey": "MYTHRAS.Skills.Dance.Title",
    "localizationDescriptionKey": "MYTHRAS.Skills.Dance.Description",
    "characteristic1": "dexterity",
    "characteristic2": "charisma"
  },
  "deceit": {
    "localizationTitleKey": "MYTHRAS.Skills.Deceit.Title",
    "localizationDescriptionKey": "MYTHRAS.Skills.Deceit.Description",
    "characteristic1": "intelligence",
    "characteristic2": "charisma"
  },
  "drive": {
    "localizationTitleKey": "MYTHRAS.Skills.Drive.Title",
    "localizationDescriptionKey": "MYTHRAS.Skills.Drive.Description",
    "characteristic1": "dexterity",
    "characteristic2": "power"
  },
  "endurance": {
    "localizationTitleKey": "MYTHRAS.Skills.Endurance.Title",
    "localizationDescriptionKey": "MYTHRAS.Skills.Endurance.Description",
    "characteristic1": "constitution",
    "characteristic2": "constitution"
  },
  "evade": {
    "localizationTitleKey": "MYTHRAS.Skills.Evade.Title",
    "localizationDescriptionKey": "MYTHRAS.Skills.Evade.Description",
    "characteristic1": "dexterity",
    "characteristic2": "dexterity"
  },
  "first-aid": {
    "localizationTitleKey": "MYTHRAS.Skills.FirstAid.Title",
    "localizationDescriptionKey": "MYTHRAS.Skills.FirstAid.Description",
    "characteristic1": "dexterity",
    "characteristic2": "intelligence"
  },
  "influence": {
    "localizationTitleKey": "MYTHRAS.Skills.Influence.Title",
    "localizationDescriptionKey": "MYTHRAS.Skills.Influence.Description",
    "characteristic1": "charisma",
    "characteristic2": "charisma"
  },
  "insight": {
    "localizationTitleKey": "MYTHRAS.Skills.Insight.Title",
    "localizationDescriptionKey": "MYTHRAS.Skills.Insight.Description",
    "characteristic1": "intelligence",
    "characteristic2": "power"
  },
  "locale": {
    "localizationTitleKey": "MYTHRAS.Skills.Locale.Title",
    "localizationDescriptionKey": "MYTHRAS.Skills.Locale.Description",
    "characteristic1": "intelligence",
    "characteristic2": "intelligence"
  },
  "native-tongue": {
    "localizationTitleKey": "MYTHRAS.Skills.NativeTongue.Title",
    "localizationDescriptionKey": "MYTHRAS.Skills.NativeTongue.Description",
    "characteristic1": "intelligence",
    "characteristic2": "charisma"
  },
  "perception": {
    "localizationTitleKey": "MYTHRAS.Skills.Perception.Title",
    "localizationDescriptionKey": "MYTHRAS.Skills.Perception.Description",
    "characteristic1": "intelligence",
    "characteristic2": "power"
  },
  "ride": {
    "localizationTitleKey": "MYTHRAS.Skills.Ride.Title",
    "localizationDescriptionKey": "MYTHRAS.Skills.Ride.Description",
    "characteristic1": "dexterity",
    "characteristic2": "power"
  },
  "sing": {
    "localizationTitleKey": "MYTHRAS.Skills.Sing.Title",
    "localizationDescriptionKey": "MYTHRAS.Skills.Sing.Description",
    "characteristic1": "power",
    "characteristic2": "charisma"
  },
  "stealth": {
    "localizationTitleKey": "MYTHRAS.Skills.Stealth.Title",
    "localizationDescriptionKey": "MYTHRAS.Skills.Stealth.Description",
    "characteristic1": "dexterity",
    "characteristic2": "intelligence"
  },
  "swim": {
    "localizationTitleKey": "MYTHRAS.Skills.Swim.Title",
    "localizationDescriptionKey": "MYTHRAS.Skills.Swim.Description",
    "characteristic1": "strength",
    "characteristic2": "constitution"
  },
  "unarmed": {
    "localizationTitleKey": "MYTHRAS.Skills.Unarmed.Title",
    "localizationDescriptionKey": "MYTHRAS.Skills.Unarmed.Description",
    "characteristic1": "strength",
    "characteristic2": "constitution"
  },
  "willpower": {
    "localizationTitleKey": "MYTHRAS.Skills.Willpower.Title",
    "localizationDescriptionKey": "MYTHRAS.Skills.Willpower.Description",
    "characteristic1": "power",
    "characteristic2": "power"
  },
  "combat-style": {
    "localizationTitleKey": "MYTHRAS.Skills.CombatStyle.Title",
    "localizationDescriptionKey": "MYTHRAS.Skills.CombatStyle.Description",
    "characteristic1": "strength",
    "characteristic2": "dexterity"
  }
};

/* -------------------------------------------- */

MYTHRAS.skillDifficulties = {
  "veryEasy": {
    "localizationKey" : "MYTHRAS.Skills.VeryEasy"
  },
  "easy": {
    "localizationKey" : "MYTHRAS.Skills.Easy"
  },
  "standard": {
    "localizationKey" : "MYTHRAS.Skills.Standard"
  },
  "hard": {
    "localizationKey" : "MYTHRAS.Skills.Hard"
  },
  "formidable": {
    "localizationKey" : "MYTHRAS.Skills.Formidable"
  },
  "herculean": {
    "localizationKey" : "MYTHRAS.Skills.Herculean"
  }
};

/* -------------------------------------------- */

MYTHRAS.spellPreparationModes = {
  "always": "MYTHRAS.SpellPrepAlways",
  "atwill": "MYTHRAS.SpellPrepAtWill",
  "innate": "MYTHRAS.SpellPrepInnate",
  "pact": "MYTHRAS.PactMagic",
  "prepared": "MYTHRAS.SpellPrepPrepared"
};

MYTHRAS.spellUpcastModes = ["always", "pact", "prepared"];


MYTHRAS.spellProgression = {
  "none": "MYTHRAS.SpellNone",
  "full": "MYTHRAS.SpellProgFull",
  "half": "MYTHRAS.SpellProgHalf",
  "third": "MYTHRAS.SpellProgThird",
  "pact": "MYTHRAS.SpellProgPact",
  "artificer": "MYTHRAS.SpellProgArt"
};

/* -------------------------------------------- */

/**
 * The available choices for how spell damage scaling may be computed
 * @type {Object}
 */
MYTHRAS.spellScalingModes = {
  "none": "MYTHRAS.SpellNone",
  "cantrip": "MYTHRAS.SpellCantrip",
  "level": "MYTHRAS.SpellLevel"
};

/* -------------------------------------------- */


/**
 * Define the set of types which a weapon item can take
 * @type {Object}
 */
MYTHRAS.weaponTypes = {
  "simpleM": "MYTHRAS.WeaponSimpleM",
  "simpleR": "MYTHRAS.WeaponSimpleR",
  "martialM": "MYTHRAS.WeaponMartialM",
  "martialR": "MYTHRAS.WeaponMartialR",
  "natural": "MYTHRAS.WeaponNatural",
  "improv": "MYTHRAS.WeaponImprov"
};


/* -------------------------------------------- */

/**
 * Define the set of weapon property flags which can exist on a weapon
 * @type {Object}
 */
MYTHRAS.weaponProperties = {
  "amm": "MYTHRAS.WeaponPropertiesAmm",
  "hvy": "MYTHRAS.WeaponPropertiesHvy",
  "fin": "MYTHRAS.WeaponPropertiesFin",
  "fir": "MYTHRAS.WeaponPropertiesFir",
  "foc": "MYTHRAS.WeaponPropertiesFoc",
  "lgt": "MYTHRAS.WeaponPropertiesLgt",
  "lod": "MYTHRAS.WeaponPropertiesLod",
  "rch": "MYTHRAS.WeaponPropertiesRch",
  "rel": "MYTHRAS.WeaponPropertiesRel",
  "ret": "MYTHRAS.WeaponPropertiesRet",
  "spc": "MYTHRAS.WeaponPropertiesSpc",
  "thr": "MYTHRAS.WeaponPropertiesThr",
  "two": "MYTHRAS.WeaponPropertiesTwo",
  "ver": "MYTHRAS.WeaponPropertiesVer"
};


// Spell Components
MYTHRAS.spellComponents = {
  "V": "MYTHRAS.ComponentVerbal",
  "S": "MYTHRAS.ComponentSomatic",
  "M": "MYTHRAS.ComponentMaterial"
};

// Spell Schools
MYTHRAS.spellSchools = {
  "abj": "MYTHRAS.SchoolAbj",
  "con": "MYTHRAS.SchoolCon",
  "div": "MYTHRAS.SchoolDiv",
  "enc": "MYTHRAS.SchoolEnc",
  "evo": "MYTHRAS.SchoolEvo",
  "ill": "MYTHRAS.SchoolIll",
  "nec": "MYTHRAS.SchoolNec",
  "trs": "MYTHRAS.SchoolTrs"
};

// Spell Levels
MYTHRAS.spellLevels = {
  0: "MYTHRAS.SpellLevel0",
  1: "MYTHRAS.SpellLevel1",
  2: "MYTHRAS.SpellLevel2",
  3: "MYTHRAS.SpellLevel3",
  4: "MYTHRAS.SpellLevel4",
  5: "MYTHRAS.SpellLevel5",
  6: "MYTHRAS.SpellLevel6",
  7: "MYTHRAS.SpellLevel7",
  8: "MYTHRAS.SpellLevel8",
  9: "MYTHRAS.SpellLevel9"
};

// Spell Scroll Compendium UUIDs
MYTHRAS.spellScrollIds = {
  0: 'Compendium.mythras.items.rQ6sO7HDWzqMhSI3',
  1: 'Compendium.mythras.items.9GSfMg0VOA2b4uFN',
  2: 'Compendium.mythras.items.XdDp6CKh9qEvPTuS',
  3: 'Compendium.mythras.items.hqVKZie7x9w3Kqds',
  4: 'Compendium.mythras.items.DM7hzgL836ZyUFB1',
  5: 'Compendium.mythras.items.wa1VF8TXHmkrrR35',
  6: 'Compendium.mythras.items.tI3rWx4bxefNCexS',
  7: 'Compendium.mythras.items.mtyw4NS1s7j2EJaD',
  8: 'Compendium.mythras.items.aOrinPg7yuDZEuWr',
  9: 'Compendium.mythras.items.O4YbkJkLlnsgUszZ'
};

/**
 * Define the standard slot progression by character level.
 * The entries of this array represent the spell slot progression for a full spell-caster.
 * @type {Array[]}
 */
MYTHRAS.SPELL_SLOT_TABLE = [
  [2],
  [3],
  [4, 2],
  [4, 3],
  [4, 3, 2],
  [4, 3, 3],
  [4, 3, 3, 1],
  [4, 3, 3, 2],
  [4, 3, 3, 3, 1],
  [4, 3, 3, 3, 2],
  [4, 3, 3, 3, 2, 1],
  [4, 3, 3, 3, 2, 1],
  [4, 3, 3, 3, 2, 1, 1],
  [4, 3, 3, 3, 2, 1, 1],
  [4, 3, 3, 3, 2, 1, 1, 1],
  [4, 3, 3, 3, 2, 1, 1, 1],
  [4, 3, 3, 3, 2, 1, 1, 1, 1],
  [4, 3, 3, 3, 3, 1, 1, 1, 1],
  [4, 3, 3, 3, 3, 2, 1, 1, 1],
  [4, 3, 3, 3, 3, 2, 2, 1, 1]
];

/* -------------------------------------------- */

// Polymorph options.
MYTHRAS.polymorphSettings = {
  keepPhysical: 'MYTHRAS.PolymorphKeepPhysical',
  keepMental: 'MYTHRAS.PolymorphKeepMental',
  keepSaves: 'MYTHRAS.PolymorphKeepSaves',
  keepSkills: 'MYTHRAS.PolymorphKeepSkills',
  mergeSaves: 'MYTHRAS.PolymorphMergeSaves',
  mergeSkills: 'MYTHRAS.PolymorphMergeSkills',
  keepClass: 'MYTHRAS.PolymorphKeepClass',
  keepFeats: 'MYTHRAS.PolymorphKeepFeats',
  keepSpells: 'MYTHRAS.PolymorphKeepSpells',
  keepItems: 'MYTHRAS.PolymorphKeepItems',
  keepBio: 'MYTHRAS.PolymorphKeepBio',
  keepVision: 'MYTHRAS.PolymorphKeepVision'
};

/* -------------------------------------------- */

/**
 * Skill, ability, and tool proficiency levels
 * Each level provides a proficiency multiplier
 * @type {Object}
 */
MYTHRAS.proficiencyLevels = {
  0: "MYTHRAS.NotProficient",
  1: "MYTHRAS.Proficient",
  0.5: "MYTHRAS.HalfProficient",
  2: "MYTHRAS.Expertise"
};

/* -------------------------------------------- */


// Condition Types
MYTHRAS.conditionTypes = {
  "blinded": "MYTHRAS.ConBlinded",
  "charmed": "MYTHRAS.ConCharmed",
  "deafened": "MYTHRAS.ConDeafened",
  "diseased": "MYTHRAS.ConDiseased",
  "exhaustion": "MYTHRAS.ConExhaustion",
  "frightened": "MYTHRAS.ConFrightened",
  "grappled": "MYTHRAS.ConGrappled",
  "incapacitated": "MYTHRAS.ConIncapacitated",
  "invisible": "MYTHRAS.ConInvisible",
  "paralyzed": "MYTHRAS.ConParalyzed",
  "petrified": "MYTHRAS.ConPetrified",
  "poisoned": "MYTHRAS.ConPoisoned",
  "prone": "MYTHRAS.ConProne",
  "restrained": "MYTHRAS.ConRestrained",
  "stunned": "MYTHRAS.ConStunned",
  "unconscious": "MYTHRAS.ConUnconscious"
};

// Languages
MYTHRAS.languages = {
  "common": "MYTHRAS.LanguagesCommon",
  "aarakocra": "MYTHRAS.LanguagesAarakocra",
  "abyssal": "MYTHRAS.LanguagesAbyssal",
  "aquan": "MYTHRAS.LanguagesAquan",
  "auran": "MYTHRAS.LanguagesAuran",
  "celestial": "MYTHRAS.LanguagesCelestial",
  "deep": "MYTHRAS.LanguagesDeepSpeech",
  "draconic": "MYTHRAS.LanguagesDraconic",
  "druidic": "MYTHRAS.LanguagesDruidic",
  "dwarvish": "MYTHRAS.LanguagesDwarvish",
  "elvish": "MYTHRAS.LanguagesElvish",
  "giant": "MYTHRAS.LanguagesGiant",
  "gith": "MYTHRAS.LanguagesGith",
  "gnomish": "MYTHRAS.LanguagesGnomish",
  "goblin": "MYTHRAS.LanguagesGoblin",
  "gnoll": "MYTHRAS.LanguagesGnoll",
  "halfling": "MYTHRAS.LanguagesHalfling",
  "ignan": "MYTHRAS.LanguagesIgnan",
  "infernal": "MYTHRAS.LanguagesInfernal",
  "orc": "MYTHRAS.LanguagesOrc",
  "primordial": "MYTHRAS.LanguagesPrimordial",
  "sylvan": "MYTHRAS.LanguagesSylvan",
  "terran": "MYTHRAS.LanguagesTerran",
  "cant": "MYTHRAS.LanguagesThievesCant",
  "undercommon": "MYTHRAS.LanguagesUndercommon"
};

// Character Level XP Requirements
MYTHRAS.CHARACTER_EXP_LEVELS =  [
  0, 300, 900, 2700, 6500, 14000, 23000, 34000, 48000, 64000, 85000, 100000,
  120000, 140000, 165000, 195000, 225000, 265000, 305000, 355000]
;

// Challenge Rating XP Levels
MYTHRAS.CR_EXP_LEVELS = [
  10, 200, 450, 700, 1100, 1800, 2300, 2900, 3900, 5000, 5900, 7200, 8400, 10000, 11500, 13000, 15000, 18000,
  20000, 22000, 25000, 33000, 41000, 50000, 62000, 75000, 90000, 105000, 120000, 135000, 155000
];

// Configure Optional Character Flags
MYTHRAS.characterFlags = {
  "powerfulBuild": {
    name: "MYTHRAS.FlagsPowerfulBuild",
    hint: "MYTHRAS.FlagsPowerfulBuildHint",
    section: "Racial Traits",
    type: Boolean
  },
  "savageAttacks": {
    name: "MYTHRAS.FlagsSavageAttacks",
    hint: "MYTHRAS.FlagsSavageAttacksHint",
    section: "Racial Traits",
    type: Boolean
  },
  "elvenAccuracy": {
    name: "MYTHRAS.FlagsElvenAccuracy",
    hint: "MYTHRAS.FlagsElvenAccuracyHint",
    section: "Racial Traits",
    type: Boolean
  },
  "halflingLucky": {
    name: "MYTHRAS.FlagsHalflingLucky",
    hint: "MYTHRAS.FlagsHalflingLuckyHint",
    section: "Racial Traits",
    type: Boolean
  },
  "initiativeAdv": {
    name: "MYTHRAS.FlagsInitiativeAdv",
    hint: "MYTHRAS.FlagsInitiativeAdvHint",
    section: "Feats",
    type: Boolean
  },
  "initiativeAlert": {
    name: "MYTHRAS.FlagsAlert",
    hint: "MYTHRAS.FlagsAlertHint",
    section: "Feats",
    type: Boolean
  },
  "jackOfAllTrades": {
    name: "MYTHRAS.FlagsJOAT",
    hint: "MYTHRAS.FlagsJOATHint",
    section: "Feats",
    type: Boolean
  },
  "observantFeat": {
    name: "MYTHRAS.FlagsObservant",
    hint: "MYTHRAS.FlagsObservantHint",
    skills: ['prc','inv'],
    section: "Feats",
    type: Boolean
  },
  "reliableTalent": {
    name: "MYTHRAS.FlagsReliableTalent",
    hint: "MYTHRAS.FlagsReliableTalentHint",
    section: "Feats",
    type: Boolean
  },
  "remarkableAthlete": {
    name: "MYTHRAS.FlagsRemarkableAthlete",
    hint: "MYTHRAS.FlagsRemarkableAthleteHint",
    abilities: ['str','dex','con'],
    section: "Feats",
    type: Boolean
  },
  "weaponCriticalThreshold": {
    name: "MYTHRAS.FlagsCritThreshold",
    hint: "MYTHRAS.FlagsCritThresholdHint",
    section: "Feats",
    type: Number,
    placeholder: 20
  }
};

// Configure allowed status flags
MYTHRAS.allowedActorFlags = [
  "isPolymorphed", "originalActor"
].concat(Object.keys(MYTHRAS.characterFlags));
