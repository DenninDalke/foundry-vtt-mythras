/**
 * Define a set of template paths to pre-load
 * Pre-loaded templates are compiled and cached for fast access when rendering
 * @return {Promise}
 */
export const preloadHandlebarsTemplates = async function() {

  // Define template paths to load
  const templatePaths = [

    // Actor Sheet Partials
    "systems/mythras/templates/actors/parts/actor-traits.html",
    "systems/mythras/templates/actors/parts/actor-health-armor.html",
    "systems/mythras/templates/actors/parts/actor-inventory.html",
    "systems/mythras/templates/actors/parts/actor-skills.html",
    "systems/mythras/templates/actors/parts/actor-features.html",
    "systems/mythras/templates/actors/parts/actor-spellbook.html",

    // Item Sheet Partials
    "systems/mythras/templates/items/parts/item-action.html",
    "systems/mythras/templates/items/parts/item-activation.html",
    "systems/mythras/templates/items/parts/item-description.html"
  ];

  // Load the template parts
  return loadTemplates(templatePaths);
};
